<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Task;
class taskController extends Controller
{
    public function getall(){
        $task = new Task();
        $tasks = $task->get();
        return view('taskboard')->with('tasks',$tasks);
    }
    public function addtaskform(int $id){
        return view('addtask')->with('id',$id);
    }
    
    public function make_safe($variable) {
      $variable = strip_tags($variable);
      $variable = stripslashes($variable);
      $variable= trim($variable, "'");
        return $variable;
        
    }
    
    public function create(Request $request,int $id){
        $taskname = $request->input('name');
        $taskdesc = $request->input('description');
        $groupid = $id;
        $taskdate = $request->input('date');
        $taskstatus = $request->input('status');
        $task  = new Task();
        $task->taskname = $this->make_safe($taskname);
        $task->taskdesc = $this->make_safe($taskdesc);
        $task->taskdate = $this->make_safe($taskdate);
        $task->groupid = $this->make_safe($groupid);
        $task->status = $this->make_safe($taskstatus);
        $task->save();
        $res = array();
        $res["fail"] = "0";
        $res["type"] = "add";
        $res["task"] = $task;
        echo json_encode($res);
    }
    public function edit(string $id){
        $task = Task::find($id);
        $res = array();
        $res['fail']='0';
        $res['task']=$task;
        echo json_encode($res);
    }
    public function update(Request $request,int $id){
        $task = Task::find($id);
        $taskname = $request->input('name');
        $taskdesc = $request->input('description');
        $groupid = $id;
        $taskdate = $request->input('date');
        $taskstatus = $request->input('status');
     
        $task->taskname = $this->make_safe($taskname);
        $task->taskdesc = $this->make_safe($taskdesc);
        $task->taskdate = $this->make_safe($taskdate);
        $task->status = $this->make_safe($taskstatus);
        $task->save();
        $res = array();
        $res["fail"] = "0";
        $res["type"] = "edit";
        $res['task'] = $task;
        echo json_encode($res);
    }
}
