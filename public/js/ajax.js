var frm = $('#modalForm');
frm.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            dataType: 'json',
            success: function (response) {
                if(response.fail === '0' && response.type === 'add'){
                $(".success").show().delay(3000).hide(0);
                 var id = response.task.id;
                    var name = response.task.taskname;
                    var date = response.task.taskdate;
                    var status = response.task.status;
                var color = getcolor(status);
                   $('#table'+response.task.groupid+' > tbody:last-child').append('<tr id="tr'+id+'" > <td class="columnwidth sort" ><button  class="modalbtn editmodal" id="taskname'+id+'" onClick="getTask('+id+')">'+ name+' </button></td><td id="taskdate'+id+'"> '+date+' </td> <td id="status'+id+'" style="color: '+color+'; font-weight: 500">'+status+'</td> </tr>');
                    }
            else if(response.fail === '0' && response.type === 'add'){
                $(".danger").show().delay(3000).hide(0);
                 }
            else if(response.fail === '0' && response.type === 'edit'){
                $(".successedit").show().delay(3000).hide(0);
                    var id = response.task.id;
                    var name = response.task.taskname;
                    var date = response.task.taskdate;
                    var status = response.task.status;
                    var color = getcolor(status);
                    $('#taskname'+id).html(name);
                    $('#taskdate'+id).html(date);
                    $('#status'+id).html(status).css('color',color);
                   
                       }
            else    {
                $(".dangeredit").show().delay(3000).hide(0);
                 }
             
                 
                 $('#modalForm').trigger("reset");
                 $('#myModal').hide();
                 
            },
            error: function () {
               $(".danger").show().delay(3000).hide(0);
            },
        });
    });
    
   
   