// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var btn2 = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
    var frm = $('.modalform');
    frm.attr('action','/addtaskaction/1');
    

}
btn2.onclick = function() {
    modal.style.display = "block";
    var frm = $('.modalform');
     frm.attr('action','/addtaskaction/2'); 
}
function getTask(id){
     modal.style.display = "block";
    var frm = $('.modalform');
    frm.attr('action','/edittaskaction/'+id);    
    frm.attr('id','edittask');  
       $.ajax({
            type: 'GET',
            url: 'edittask/'+id,
            dataType: 'json',
            success: function (response) {
               if(response.fail === '0'){
                  $('#taskname').val(response.task.taskname);
                  $('#taskdesc').val(response.task.taskdesc);
                  $('#taskdate').val(response.task.taskdate);
                  $('#taskstatus').val(response.task.status);
                }
            
            },
            error: function () {
            },
       });
     
   }
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
                 $('#modalForm').trigger("reset");
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
                 $('#modalForm').trigger("reset");
    }
}
function closemodal(){
    modal.style.display = "none";
                 $('#modalForm').trigger("reset");
   }
 function getcolor( status){
        var color ='';
        if(status === 'DONE')color ='green';
        if(status === 'TO DO')color ='red';
        if(status === 'IN PROGRESS')color ='yellow';
        if(status === 'IN REVIEW')color ='orange';
        return color;
    }
    