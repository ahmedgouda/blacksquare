<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','taskController@getall');
Route::get('addtask/{id}','taskController@addtaskform');
Route::POST('addtaskaction/{id}','taskController@create');
Route::get('edittask/{id}','taskController@edit');
Route::POST('edittaskaction/{id}','taskController@update');