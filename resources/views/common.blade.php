
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="{{asset("css/style.css")}}">
        <link rel="stylesheet" href="{{asset("css/modal.css")}}">
        <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap.css")}}">
        <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap-grid.css")}}">
        <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap-reboot.css")}}">
        <link rel="icon" href="{{asset("images/blacksquareicon.png")}}"> 
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Task dashboard</title>
       
    </head>
    
    <header class="header">
   <img src="{{asset("images/blacksquarelogo.png")}}">
   
    <div class="successedit alertsuccess" style="text-align: center;"><p>Task edited successfully</p></div>
    <div class="success alertsuccess" ><p style="text-align: center;">Task added successfully</p></div>
    </header>
    
    @yield('content')
    <footer class="footer">
        <p >
            Copyright @ 2018 Black Squares Solutions.
        </p>
    </footer>
    <script src="{{asset("bootstrap/js/bootstrap.js")}}"></script>
        <script src="{{asset("js/sortme.js")}}"></script>
        <script src="{{asset("js/jquery-3.2.1.min.js")}}"></script>
        <script src="{{asset("js/ajax.js")}}"></script>
        <script src="{{asset("js/modal.js")}}"></script>
</html>