@extends('common')
@section('content')
<body style="background-color:#f5f5f5; "> 
       <?php if(isset($task)){
           $id = $task->id;
           $taskname = $task->taskname;
           $taskdesc = $task->taskdesc;
           $taskdate = $task->taskdate;
           $status = $task->status;
       ?>
            <section class="form">
                <form method="post" action="/edittaskaction/<?php echo $id;?>" id="edittask">
                  {!! csrf_field() !!} 
                <label>Task Name :</label><br>
                <input class="form-control" style="width: 40%" type="text" name="name" value="<?php echo $taskname;?>"><br>
                <label>Task description :</label><br>
                <input class="form-control" style="width: 40%" type="text" name="description" value="<?php echo $taskdesc;?>"><br>
                <label>Task date :</label><br>
                <input class="form-control" style="width: 40%" type="date" name="date" value="<?php echo $taskdate;?>"><br>
                <label>Task status :</label><br>
                <select class="form-control" style="width: 40%" name="status" value="<?php echo $status;?>">
                    <option value="TO DO">TO DO</option>
                    <option value="IN PROGRESS">IN PROGRESS</option>
                    <option value="IN REVIEW">IN REVIEW</option>
                    <option value="DONE">DONE</option>
                </select><br>
                <input  class="btn" type="submit" value="Submit">&nbsp; 
                <button class="btn" ><a  href="/" style="color: black;">Cancel</a></button>
            </form>
            </section>
       <?php } ?>
    </body>
@stop