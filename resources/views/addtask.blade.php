@extends('common')
@section('content')
<body style="background-color:#f5f5f5; "> 
       
            <section class="form">
                <div class="success alertsuccess" ><p>Task added successfully</p></div>
                <div class="danger alertdanger" ><p>Please fill all data</p></div>
                <form method="post" action="/addtaskaction/<?php echo $id;?>" id="addtask">
                  {!! csrf_field() !!} 
                <label>Task Name :</label><br>
                <input class="form-control" style="width: 40%" type="text" name="name" ><br>
                <label>Task description :</label><br>
                <input class="form-control" style="width: 40%" type="text" name="description" ><br>
                <label>Task date :</label><br>
                <input class="form-control" style="width: 40%" type="date" name="date"><br>
                <label>Task status :</label><br>
                <select class="form-control" style="width: 40%" name="status">
                    <option value="TO DO">TO DO</option>
                    <option value="IN PROGRESS">IN PROGRESS</option>
                    <option value="IN REVIEW">IN REVIEW</option>
                    <option value="DONE">DONE</option>
                </select><br>
                <input  class="btn" type="submit" value="Submit"> &nbsp; 
                <button onclick='window.location.href="/"'  class="btn">Cancel</button>
            </form>
            </section>
    </body>
</html>
@stop